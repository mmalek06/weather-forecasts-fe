import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { ForecastModel } from "./forecast.model";

@Injectable()
export class ForecastsService {
    private endpoint: string = "http://localhost:59380/api/weather/";

    constructor(private http: HttpClient) { }

    public async getForecast(country: string, city: string): Promise<ForecastModel> {
        const fullUrl = `${this.endpoint}${country}/${city}`;

        return await this.http.get<ForecastModel>(fullUrl).toPromise();
    }
}