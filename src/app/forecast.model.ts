export interface ForecastModel {
    location: LocationViewModel;
    temperature: TemperatureViewModel;
    humidity: number;
}

export interface LocationViewModel {
    country: string;
    city: string;
}

export interface TemperatureViewModel {
    format: string;
    value: number;
}