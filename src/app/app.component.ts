import { Component } from '@angular/core';

import { ForecastsService } from './forecasts.service';
import { debounce } from './utils';
import { ForecastModel } from './forecast.model';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    private city: string;
    private country: string;

    public weatherConditions: string;
    public isLoading: boolean;

    public constructor(private forecastsSvc: ForecastsService) { }

    public cityChanged = debounce(async (city: string) => {
        this.city = city;
        
        await this.loadData();
    }, 500, false);

    public countryChanged = debounce(async (country: string) => {
        this.country = country;

        await this.loadData();
    }, 500, false);

    private async loadData() {
        if (!this.city || !this.country) {
            return;
        }

        this.isLoading = true;

        const result = await this.forecastsSvc.getForecast(this.country, this.city);
        
        this.isLoading = false;
        this.weatherConditions = this.formatResponse(result);
    }

    private formatResponse(response: ForecastModel): string {
        const result = 
            `Temperature in ${response.location.country}, ${response.location.city} is: 
            ${response.temperature.value} degrees ${response.temperature.format} and 
            humidity is ${response.humidity}`;

        return result;
    }
}
